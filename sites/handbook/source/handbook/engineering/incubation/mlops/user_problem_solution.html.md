---
layout: handbook-page-toc
title: "MLOps User Problems"
description: "A quick lookup matching MLOps User Problems with solutions"
canonical_path: "/direction/learn/mlops/primer"
noindex: false
---

# MLOps User Problem to Solution

This page is meant to be a quick guide on matching user problems to possible 
we can explore in the MLOps space. 

## User Problems

- [Experimentation Pipeline](#experimentation-pipeline)
    1. The Data Scientists in my org run model training on their own machines
- [CI/CD](#cicd)
    1. Training models is very manual and ineficient
- [Model Registry](#model-registry)
    1. We are having issues keeping track of the models that are in production
    1. I don't understand if an iteration of a model is actually better than the old version
- [Feature Store](#feature-store)
    1. We have many models online, but each seems to have their own definition of features, and it's becoming hard to track data quality
- [Insights Store](#insights-store)
    1. Our Data Scientists create analysis and studies for the business, but we have trouble sharing those insights
    1. Our Data Scientists create analysis and studies for the business, but we have trouble finding what was done in the past
- [Labeling Service](#labeling-service)
    1. I can't find data to train our models
    1. We can't keep track of the data we already have annotated
    1. We have a group of people to label our examples, but we are unsure of the quality
    1. We have a group of people to label our examples, but it is still very slow
- [Prediction Service](#prediction-service)
- [Model Monitoring](#model-monitoring)

## MLOps Solutions

### Experimentation Pipeline

On MLOps, pipelines are not used only on the CI/CD step, but even before code hits Git. It's often necessary to run pipelines while developing the code, since
Data Scientists might need access to cloud resources and to manage their steps. For example, it's common to have already start the work with a pipeline that 
fetches data, cleans, trains the model and evaluate them.

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/5

#### Examples
- (KubeFlow)[https://www.kubeflow.org/docs/components/pipelines/overview/pipelines-overview/]
- (Amazon Sagemaker)[https://aws.amazon.com/sagemaker/pipelines/]

### CI/CD

_We will be adding this soon_

### Model Registry

Used to store, discover and verify model artefacts and model metadata. Using during model creation to track experiments (hyperparameter tuning, comparing multiple models, etc), and during production to serve the correct artifacts. Can be integrated with a prediction service to automatically provision production instances for the correct models, and with monitoring solutions to catch problems early on.

- (MLFlow)[https://mlflow.org/]
- (Netpune.ai)[https://neptune.ai]
- (AWS Sage Maker Model Registry)[https://docs.aws.amazon.com/sagemaker/latest/dg/model-registry.html]
- (Iterative.ai Studio)[https://studio.iterative.ai/]

### Model Monitoring

_We will be adding this soon_

### Feature Store

Feature Store is a services that centralizes access to features, along with transformations and metadata definitions. This way, when creating a new model, Data Scientists can search for already existing features and build upon them, or create a new one with a library of operators. Instead of connecting to the data lake directly, during training and serving all features are retrieved directly from the Feature Store.

A feature store can also be seen as the connection point between MLOps and DataOps.

- [Tecton.ai](https://www.tecton.ai/)
- [Feast](https://github.com/feast-dev/feast) OpenSource, is now part of Kubeflow, and supported by Google as part of Vertex.ai

### Insights Store

As the number of data scientists in the team grow, so does the number of analysis and insights they generate. At some point, this knowledge is spread out
across different silos, and it becomes more and more common to see duplicated work. An Insights Store is a service that hosts analysis commonly
carried out with Jupyte Notebooks or RMarkdown and makes it easier to discover, discuss and follow previous and new work being done across the
organization.

- [Knowledge Repo](https://github.com/airbnb/knowledge-repo)

### Labeling Service

_We will be adding this soon_

### Prediction Service

_We will be adding this soon_
