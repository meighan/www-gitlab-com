---
layout: handbook-page-toc
title: "GitLab SOX ITGC Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What are ITGCs?

ITGCs, or IT General controls (sometimes referred to at GITCs - General IT controls), are a subset of the [SOX internal control set](https://internal-handbook.gitlab.io/finance/sox-internal-controls/). The objectives of ITGCs are to ensure the integrity of the data and processes that the in scope systems support. They can be applied at the application, OS, database and infrastructure layers. 

### What are GitLab's ITGCs?

In partnership with our Internal Audit and External SOX Audit teams, beginning in Q1 FY22 GitLab's ITGCs for SOX compliance are:

|**Sl #**|**Control Family**|[**GCF Control ID**](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#list-of-controls-by-family)|**ITGC Control ID**|**Control Mapping**
|:-|:--|:-|:-|:---|
|1|Access to Programs and Data|IAC-07|LA.1|Logical access provisioning requires approval from appropriate personnel.
|2|Access to Programs and Data|IAC-07|LA.2|Terminated users have their access revoked in a timely manner.
|3|Access to Programs and Data|IAC-17|LA.3|GitLab access reviews are performed on a quarterly basis; research and corrective action is taken where applicable.
|4|Access to Programs and Data|IAC-16|LA.4|The ability to add, modify, and delete accounts is limited to appropriate personnel.
|5|Access to Programs and Data|IAC-10|LA.5|Authentication to in-scope systems is configured in line with the password policy. Exemptions to the policy are formally approved.
|6|Change Management|CHG-04|PC.1|Access to migrate changes to production is limited to appropriate personnel.
|7|Change Management|CHG-02|PC.2|Changes are tested and approved by appropriate personnel in accordance with the change management policy.
|8|Computer Operations|CO.1|CO.1|Access to modify relevant jobs is restricted to appropriate personnel.
|9|Computer Operations|CO.2|CO.2|Jobs are monitored to ensure effective ongoing operation. Issues are researched and resolved in a timely manner.
|10|Computer Operations|BCD-11|CO.3|Backups are completed according to a predefined system schedule.
|11|Computer Operations|BCD-12|CO.4|GitLab performs backup restoration or failover tests at least annually to confirm the reliability and integrity of system backups or recovery operations.
|12|Program Development|PRM-07|PD.1|Significant program changes are tested and known issues are communicated to the relevant stakeholders prior to approval.
|13|Program Development|PRM-07|PD.2|GitLab validates that data transferred during an applicable program change is complete and accurate.

## Who is responsible for the ITGC project?

* Security Compliance is responsible for the creation, continuous auditing, and the advisement of audit requirements relating to ITGCs.
* Internal Audit is responsible for executing independent reliance testing on the audit work preformed by the security compliance team.
* Every GitLab team member is responsible for operating the processes required by SOX against the in scope SOX systems.
* GitLab's leadership is responsible for supporting this initiative and giving all GitLab teams the resources (e.g. time and tools) they require to implement and operate SOX processes. 

## Where can I submit feedback for the ITGC project?

Please add a comment to this [feedback issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/1097).

You can also [contact the security compliance team](/handbook/engineering/security/security-assurance/security-compliance/compliance.html) if there's any way we can help.
