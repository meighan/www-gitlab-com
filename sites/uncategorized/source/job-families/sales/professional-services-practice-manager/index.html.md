---
layout: job_family_page
title: "Professional Services Practice Manager"
---

## Professional Services Practice Manager

#### Job Grade 

The Professional Services Practice Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Define, scope, and prioritize GitLab’s services and training offerings, including persona and market segmentation analysis, offering definition, scope, and pricing
* Collaborate with subject matter experts in the field, Product Management & Marketing, and instructional designers (as appropriate) to develop and deliver prioritized global services and training offerings, content, and sales & marketing collateral
* Ensure delivery model is focused on customer success outcomes while championing quality and efficiency
* Measure and report on the effectiveness of services and training enablement investments and programs
* Partner with sales team to drive bookings and closure of services and training engagements
* Ensure a robust closed feedback loop that embraces continuous improvement and iteration
* Identify and act on opportunities to improve the customer experience via innovative services/training offerings
* Build business case for additional services and training and enablement resources as needed

#### Requirements

* Knowledge and familiarity with the Software Development Life Cycle and DevOps required (open source software knowledge and familiarity considered a plus)
* Demonstrated progressive relevant experience defining, developing, and executing customer services and/or training strategies, operations and action plans
* Ability to quickly understand technical concepts and explain them to customer and professional services audiences (mostly technical)
* Proven ability to effectively interact with and influence senior executives and team members
* Exceptional written/verbal communication and presentation skills
* Team player with strong interpersonal skills, skilled at project management and cross-functional collaboration
* Experienced in giving and receiving constructive feedback
* Ability to thrive in a fast-paced, unpredictable environment
* Share and work in accordance with GitLab's values
* Experience growing within a small start-up is a plus
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab
* Ability to travel if needed and comply with the company’s [travel policy](https://about.gitlab.com/handbook/travel/)

#### Performance Indicators

* [Bookings attached rate per agreed plan](/handbook/sales/#pcv)
* [Services bookings and revenue per agreed plan](/handbook/sales/#pcv)

### Specialties

### Professional Services & Consulting

#### Responsibilities

- Serve in a player/coach role balancing field delivery with operations and strategy
- Establish strategic plans and operational practices (e.g., people, skill sets, engagement approaches, processes and policies, enablement and coaching) for existing and new services
- Develop collateral to clearly describe and communicate the customer value of the service offerings (i.e., scope, deliverables, pricing)
- Successfully introduce and incubate new services to deliver on key success measures, including customer-facing engagements and internal initiatives
- Partner with the sales teams to build services forecasts for new and existing services
- Help to manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement process and tooling improvements to help project managers and consultants better support external customer professional services requirements
- Coach, motivate and train Professional Services Engineer team members and consultants to improve their delivery quality and efficiency
- Work with the Product Development and Support teams to contribute documentation for GitLab

### Training & Certification

#### Responsibilities

* Develop, implement and monitor training programs.
* Evaluate needs of the company and align training with the organization’s strategic plans.
* Create training materials, online learning modules, presentations and multimedia visual aids.
* Evaluates the effectiveness of training based upon formal and informal feedback from customers and end users
* Conducts training sessions and develops criteria for evaluating the effectiveness of training activities
* Implements and manages a process to ensure that all training documentation, reports and data are completed and that materials are delivered per set guidelines
* Maintains and updates records of training activities, participant progress, and program effectiveness
* Maintains and updates skills by researching new training, educational, and multimedia technologies
* Regularly evaluates trainer’s professional skills and abilities and provides coaching and counseling as needed
* Build and manage infrastructure to support the training program.
* Assist with developing strategic plans to address the organization’s long-term training and career development needs.
* Supervise, coach and mentor staff.
* Collaborate with other departmental heads to define training needs and skills gaps.

#### Requirements

* Bachelor’s degree in business, management, education or related field.
* Demonstrated progressive experience managing, specifically creating, training programs.
* Comprehensive knowledge of the principles, methods, and techniques used in the development and delivery of training programs
* Comprehensive knowledge of relevant training technologies, such as Learning Management Systems (LMS).
* Strong computer skills. Proficiency in Word, Excel and PowerPoint required. A working knowledge of core business systems preferred
* Excellent written and verbal communication skills with the ability to focus and clarify concepts
* Demonstrated problem solving and decision-making abilities with effective organizational and time management skills; the ability to handle multiple projects and priorities effectively in a fast-paced environment with minimal supervision
* Strong organizational, multi-tasking and presentation skills. Ability to create momentum and foster organizational change
* Must exhibit initiative, decisiveness and creativity, along with self-motivation and the ability to assume responsibility and maintain strict confidentiality
* Proven ability to conduct training needs assessments with key stakeholders.
* Proven ability to develop training curriculum and deliver effective and satisfying learning experiences.
* Experience with coaching and mentoring team members.
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab

### Levels

## Manager, Professional Services Practice Management 

The Manager, Professional Services Practice Management reports into the Senior Director, Professional Services.

#### Job Grade 

The Manager, Professional Services Practice Management is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Work as part of the Professional Service Leadership team to build a portfolio of service offerings, including development of new service offerings and performance of existing offerings as measured by bookings, revenue, delivery reliability, and customer satisfaction
* Provide guidance to the development of thought leadership, event-specific, and customer-facing offering and presentations
* Share hands-on technical preparation and presentation work for key accounts helping sell on the value of what GitLab professional services has to offer
* Ensure development of complete offers to ensure the services teams (i.e., partner and GitLab) can successfully deliver the offering, including systems, tools, processes, and sales enablement content.
* Establish strategic plans along with other business leaders and maintain strong relationships with solutions delivery, the sales team, client and account managers and other business leaders in support of cross-product businesses
* Develop processes that Professional Services team can continually leverage and grow to become trusted advisors to customers
* Identify and implement improvements to the processes and tools used by project teams and partners in support of external customers
* Work together with our sales organization to propose, scope, and price professional services offerings, including leading the GTM (Engagement Manager team)
* Build and scale global service methodology (i.e., people, process, systems and tools) to deliver a best-in-class customer experience while building an effective and efficient delivery processes
* Provide leadership and guidance to coach, motivate and lead services team members to their optimum performance levels and career development
* Ensure delivery model is focused on quality, cost effective delivery of services, and customer success outcomes
* Remains knowledgeable and up-to-date on GitLab releases
* Documents services provided to customers, including new code, techniques, and processes, in such a way as to make such services more efficient in the future and to add to the GitLab community
* Works with the Product Engineering and Support teams, to contribute documentation for GitLab

#### Requirements

All requirements for the Professional Services Practice Manager role apply to the Manager, Professional Services Practice Management role, as the management role will need to understand and participate with the day-to-day aspects of the Professional Services organization in addition to managing the team. 

Additional requirements include:
* 3+ years of experience managing, leading and/or delivering professional services or customer success
* Proven ability to develop strategies, translate them into initiatives and track successful delivery results including growth and profitability on services offers and customer satisfaction
* Able to adapt to environmental change and retrospecting on success and failures
* Possesses a strong management presence and leadership ability, with communication and interpersonal skills that inspire and motivate leaders and team
* Experienced in collaborating with other managers and executing strategies
* Proven track record in software/technology sales or consulting and management

## Senior Manager, Professional Services Practice Management

The Senior Manager, Professional Services Practice Management reports into the Senior Director, Professional Services.

#### Job Grade 

The Senior Manager, Professional Services Practice Management is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities 

* Extends that of the Manager, Professional Services Practice Management responsibilities
* Define and deliver strategies and plans that enhance and integrate Professional Services into GitLab's Go-To-Market strategies and plans, including direct and channel services
* Build market-specific strategies and plans based on industry best practices to improve efficiency and growth
* Manage and nurture executive relationships with customers, including management of executive-level escalations
* Be accountable for services financial goals and metrics, including revenue, gross margin and utilization.
* Own operational metrics, such as time-to-value, customer satisfaction and on-time completion of projects.

#### Requirements

* Extends that of the Manager, Professional Services Practice Management requirements
* 5+ years of experience managing, leading and/or delivering professional services or customer success
* 3+ years experience with a subscription-based business model, delivering on-premises and SaaS solutions
* Experience managing technical, cross-functional professional services teams (e.g., consulting, implementation, trainers, project managers) and delivery partners


### Career Ladder

The next steps for the Professional Services Practice Manager Job Family would be to move to the [Director, Professional Services](/job-families/sales/director-of-professional-services/) Job family.

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

1. Phone screen with a GitLab Recruiting Team Member 
2. Video Interview with the Hiring Manager
3. Team Interviews with 1-4 Team Members

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
