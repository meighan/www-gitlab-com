---
layout: markdown_page
title: "Category Direction - Metrics"
description: "Metrics help users understand how their applications are performing based on instrumented data collection. This is GitLab’s direction on where we are headed with Metrics."
canonical_path: "/direction/monitor/apm/metrics/"
---

- TOC
{:toc}

## Overview

Metrics help service operators understand the health and status of the provided services; as such, metrics are essential for ensuring the reliability and stability of those services. 

In practice, metrics are represented by a name, a type, and a measurement - which is often a numerical representation of data. Metrics can tell you simple things like the current CPU usage rate, and they can also represent more complex concepts such as an [Apdex score](https://en.wikipedia.org/wiki/Apdex).

There are many metrics that are exposed by your operating system or frameworks (e.g. Kubernetes metrics). These metrics are typically easy to collect. For other components, such as the applications you developed, you may have to add code or other interfaces (such as an agent running on the same host as your application) to expose the metrics that you care about. Exposing metrics is sometimes called [instrumentation](https://en.wikipedia.org/wiki/Instrumentation_(computer_programming)), the collection of metrics from an endpoint is called scraping.

Once you have started collecting metrics of your system, you now have the basis to define[service level indicators](https://en.wikipedia.org/wiki/Service_level_indicator) and [service level objectives](https://en.wikipedia.org/wiki/Service-level_objective). Having SLIs and SLOs is critical for efficient alerting and incident remediation. Defining SLIs and SLOs requires time to understand, correlate, and fine-tune relationships between the different kinds of metrics you are capturing.

## Current experience

Metrics is a key capability of Opstrace, the GitLab Observability distribution. Opstrace allows the user to set up Metrics by leveraging Prometheus, Cortex, commodity storage, such as S3, and Grafana. You can get started quickly by pointing your exporters at your Opstrace Prometheus endpoint.

## What's Next & Why
We are working on making observability an integrated experience within GitLab as part of the initial product integration between Opstrace and GitLab.
