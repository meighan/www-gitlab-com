document.addEventListener("DOMContentLoaded", () => {
  trackProductImpressions();
});

const productInfo = [
  {
    name: "Premium",
    id: "0002",
    brand: "GitLab",
    category: "DevOps",
  },
  {
    name: "Ultimate",
    id: "0001",
    price: "99",
    brand: "GitLab",
    category: "DevOps",
  },
];

function trackProductImpressions() {
  if (window.dataLayer) {
    window.dataLayer.push({
      event: "EECproductView",
      ecommerce: {
        currencyCode: "USD",
        impressions: productInfo 
      },
    });
  }
}
